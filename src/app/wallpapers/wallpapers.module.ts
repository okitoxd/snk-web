import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WallpapersComponent } from './wallpapers.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: WallpapersComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WallpapersComponent]
})
export class WallpapersModule { }
