import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ApiService {

  constructor(private http: Http) { }

  public getAnimeT1() {
    return this.http.get('/assets/animeT1.json')
      .map(res => res.json());
  }
  public getAnimeT2() {
    return this.http.get('/assets/animeT2.json')
      .map(res => res.json());
  }
}
