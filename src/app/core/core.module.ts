import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellComponent } from './shell/shell.component';
import { SidebarComponent } from './shell/sidebar/sidebar.component';
import { MaterializeModule } from 'ng2-materialize';
import { NavbarComponent } from './shell/navbar/navbar.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DisqusModule } from 'angular2-disqus';

const routes: Routes = [
  {
    path: '',
    loadChildren: '../home/home.module#HomeModule'
  },
  {
    path: 'anime',
    loadChildren: '../anime/anime.module#AnimeModule'
  },
  {
    path: 'manga',
    loadChildren: '../manga/manga.module#MangaModule'
  },
  {
    path: 'wallpapers',
    loadChildren: '../wallpapers/wallpapers.module#WallpapersModule'
  },
  {
    path: 'music',
    loadChildren: '../music/music.module#MusicModule'
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    DisqusModule,
    RouterModule.forRoot(routes),
    MaterializeModule.forRoot()
  ],
  exports: [ShellComponent],
  declarations: [ShellComponent, SidebarComponent, NavbarComponent]
})
export class CoreModule { }
