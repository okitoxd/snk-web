import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'snk-anime',
  templateUrl: './anime.component.html',
  styleUrls: ['./anime.component.sass']
})
export class AnimeComponent implements OnInit {

  private animeT1;
  private animeT2;

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getAnimeT1().subscribe(
      data => this.animeT1 = data,
      err => console.error(err)
    );
    this.api.getAnimeT2().subscribe(
      data => this.animeT2 = data,
      err => console.error(err)
    );
  }

  shortTitle(title: string) {
    if(title.length > 35){
      return (title.substring(0, 32) +"...");
    } else {
      return title;
    }
  }
}
