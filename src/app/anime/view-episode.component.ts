import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'snk-view-episode',
  templateUrl: './view-episode.component.html',
  styleUrls: ['./view-episode.component.sass']
})
export class ViewEpisodeComponent implements OnInit {

  private episode = {
    "num": null,
    "servers": null,
    "title": null
  };
  private serveActive: string;
  private id: string;

  constructor(private api: ApiService, private route: ActivatedRoute, public sanitizer: DomSanitizer) {
    this.route.params.subscribe(
      params => {
        if(params['temp'] =='1'){
          this.api.getAnimeT1().subscribe(
            capitulos => {
              this.episode = capitulos.find( cap => cap.num == params['cap']);
              this.serveActive = this.episode.servers[0].name;
              this.id = 'anime'+params['temp']+params['cap'];
            },
            err => console.error(err));
        }
        if(params['temp'] =='2'){
          this.api.getAnimeT2().subscribe(
            capitulos => {
              this.episode = capitulos.find( cap => cap.num == params['cap']);
              this.serveActive = this.episode.servers[0].name;
              this.id = 'anime'+params['temp']+params['cap'];
            },
            err => console.error(err));
        }
      }
    )
  }

  activeServe(serve: string) {
    this.serveActive = serve;
  }

  ngOnInit() {
    var disqus_config = function () {
        this.page.url = "www.snkweb.com";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = this.id; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
        var d = document, s = d.createElement('script');

        s.src = '//snk-web.disqus.com/embed.js';  // IMPORTANT: Replace EXAMPLE with your forum shortname!

        s.setAttribute('data-timestamp', (+new Date()).toString());
        (d.head || d.body).appendChild(s);
    })();
  }
}
