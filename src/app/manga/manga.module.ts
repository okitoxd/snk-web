import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MangaComponent } from './manga.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: MangaComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MangaComponent]
})
export class MangaModule { }
